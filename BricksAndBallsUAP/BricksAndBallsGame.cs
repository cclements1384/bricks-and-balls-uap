﻿using BricksAndBallsUAP.GameObjects;
using BricksAndBallsUAP.GameObjects.Screens.GameScreen;
using BricksAndBallsUAP.GameObjects.Screens.StartScreen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;

namespace BricksAndBallsUAP
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class BricksAndBallsGame : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private enum GameState
        {
            Loading = 0,
            Menu = 1,
            Running = 2,
            GameOver = 3
        }

        private GameState gameState;
        private StartScreen startMenu;
        private GameScreen gameScreen;

        private SoundEffect bgSound;
        
        public BricksAndBallsGame()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1280;
            graphics.PreferredBackBufferWidth = 720;
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            gameState = GameState.Loading;
            startMenu = new StartScreen();
            startMenu.Initalize();
            gameScreen = new GameScreen();
            gameScreen.Initalize();
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            if(gameState == GameState.Loading)
            {
                startMenu.LoadContent(Content);
                gameScreen.LoadContent(Content);
                gameState = GameState.Menu;
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            var touchCollection = TouchPanel.GetState();
            var mouseState = Mouse.GetState();
            var kbState = Keyboard.GetState();
          
            // TODO: Add your update logic here
            if (gameState == GameState.Menu)
            {
                startMenu.Update(gameTime,  ref mouseState);
                if (startMenu.startButton.WasPressed(ref mouseState))
                {
                    gameState = GameState.Running;
                    IsMouseVisible = false;
                }
            }
            if(gameState == GameState.Running)
            {
                gameScreen.Update(gameTime, kbState);
                if (gameScreen.GameOver)
                {
                    gameState = GameState.Menu;
                    IsMouseVisible = true;
                }
            }
            
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            // TODO: Add your drawing code here
            spriteBatch.Begin();

            if (gameState == GameState.Menu)
            {
                startMenu.Draw(spriteBatch);
            }
            if(gameState == GameState.Running)
            {
                gameScreen.Draw(spriteBatch);
            }
            if(gameState == GameState.GameOver)
            {
            }

            spriteBatch.End();
        }

    }
}
