﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BricksAndBallsUAP.GameObjects
{
    public class Block : GameObject
    {
        public int HitPoints { get; set; }
        public Vector2 position { get; set; }

        public int Left { get; set; }
        public int Right { get; set; }
        public int Bottom { get; set; }
        public int Top { get; set; }

    }
}
