﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;


namespace BricksAndBallsUAP.GameObjects.Screens
{
    public class Button
    {
    
        protected Rectangle _location;
        protected Texture2D _buttonTexture;
        protected int _lastTouchId;
        protected bool _pressed;
      
        public Vector2 Position { get; set; } 
        public Texture2D Texture
        {
            set
            {
                _buttonTexture = value;
                if (Position != null)
                {
                    _location = new Rectangle((int)Position.X,
                                      (int)Position.Y,
                                     _buttonTexture.Width,
                                     _buttonTexture.Height);
                }

            }
        }

        public Button(Texture2D buttonTexture,
            int xPosition,
            int yPosition)
        {
            _buttonTexture = buttonTexture;
            _location = new Rectangle(xPosition,
                yPosition,
                _buttonTexture.Width,
                _buttonTexture.Height);
        }

        public Button()
        {

        }

        public virtual bool WasPressed(ref TouchCollection touches)
        {
            foreach (var touch in touches)
            {
                if (touch.Id == _lastTouchId)
                {
                    continue;
                }
                  
                if (touch.State != TouchLocationState.Pressed)
                    continue;
                if (_location.Contains(touch.Position))
                {
                    _lastTouchId = touch.Id;
                    _pressed = true;
                    return true;
                }
            }
            _pressed = false;
            return false; ;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_buttonTexture,
                _location,
                null,
                Color.White);
        }

   

    }
}
