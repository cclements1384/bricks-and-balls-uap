﻿using BricksAndBallsUAP.GameObjects;
using System.Collections.Generic;

namespace BricksAndBallsUAP.Abstract
{
    public interface ILevelLoader
    {
        List<Block> LoadFromFile(string filePath);
    }
}
