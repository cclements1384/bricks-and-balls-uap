﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using BricksAndBallsUAP.Helpers;
using Microsoft.Xna.Framework.Graphics;

namespace BricksAndBallsUAP.GameObjects
{
    public class Level : GameObject
    {
        private const string LEVEL_FILE_PATH = "GameObjects/Levels/Level";

        private Texture2D redBrick;
        public List<Block> blocks { get; set; }

        public string LevelName;
        

        public bool IsCompleted
        {
            get { return blocks.Count <= 0; }
        }

        public override void Initalize()
        {
            var loader = new LevelLoader();
            blocks = loader.LoadFromFile(null);
        }

        public override void LoadContent(ContentManager content)
        {
            redBrick = content.Load<Texture2D>("graphics/red_brick");
        }

        public override void Draw(SpriteBatch spriteBatch)
        {

            foreach (var block in blocks)
            {
                if (block.HitPoints == 1)
                    spriteBatch.Draw(redBrick, block.position, Color.Wheat);
            }
        }

        public void LoadLevel(int levelNumber)
        {
            var path = string.Concat(LEVEL_FILE_PATH, levelNumber.ToString(), ".xml");
            var loader = new LevelLoader();
            blocks = loader.LoadFromFile(path);
        }


    }
}
