﻿using BricksAndBallsUAP.Abstract;
using BricksAndBallsUAP.GameObjects;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;


namespace BricksAndBallsUAP.Helpers
{
    public class LevelLoader : ILevelLoader
    {
        private int width = 60;
        private int height = 15;

        public List<Block> LoadFromFile(string filePath)
        {
            /* Check to see if the file exists */
            var xEml = XElement.Load(filePath);

            IEnumerable<Block> blocks =  
                (from b in xEml.Descendants("brick")
                select new Block
                {
                     HitPoints = Convert.ToInt32(b.Element("hitPoints").Value),
                     position = new Vector2(
                         float.Parse(b.Element("x").Value),
                         float.Parse(b.Element("y").Value))
                }).ToList();

            foreach (var block in blocks)
            {
                block.Left = (int)block.position.X;
                block.Right = (int)block.position.X + width;
                block.Top = (int)block.position.Y;
                block.Bottom = (int)block.position.Y + height;
            }


            return blocks.ToList<Block>();
         }

        public Level LoadFromFile2(string filePath)
        {
            /* Check to see if the file exists */
            var xEml = XElement.Load(filePath);

            var level = new Level();

            var levelName =
              from b in xEml.Descendants("levelInfo")
              select b.Element("levelname").Value;

            IEnumerable<Block> blocks =
                (from b in xEml.Descendants("brick")
                 select new Block
                 {
                     HitPoints = Convert.ToInt32(b.Element("hitPoints").Value),
                     position = new Vector2(
                          float.Parse(b.Element("x").Value),
                          float.Parse(b.Element("y").Value))
                 }).ToList();

            foreach (var block in blocks)
            {
                block.Left = (int)block.position.X;
                block.Right = (int)block.position.X + width;
                block.Top = (int)block.position.Y;
                block.Bottom = (int)block.position.Y + height;
            }

            level.blocks = blocks.ToList<Block>();

            return level;
        }

       
    }
}
