﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BricksAndBallsUAP.GameObjects
{
    public class Paddle : GameObject
    {
        private Texture2D paddle;
        private Rectangle destination;
  
        public Vector2 position;
        public Vector2 direction;
        public float speed;

        public int Left
        {
            get { return (int)(position.X);  }
        }

        public int Right
        {
            get
            {
                return (int)(position.X + paddle.Width);
            }
        }

        public int Top
        {
            get
            {
                return (int)position.Y;
            }
        }

        public int Bottom
        {
            get
            {
                return (int)position.Y + paddle.Height;
            }
           
        }

        public int Width
        {
            get
            {
                return paddle.Width;
            }
        }

        public override void Initalize()
        {
         

            position = new Vector2(
                615, 
                615);

            direction = new Vector2(0f,0f);

            speed = 8.0f;
        }

        public override void LoadContent(ContentManager content)
        {
            paddle = content.Load<Texture2D>("graphics/paddle");
            destination = new Rectangle(
                (int)position.X,
                (int)position.Y,
                paddle.Width,
                paddle.Height);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(paddle, position, Color.Wheat);
        }

        public  void Update(GameTime gameTime, KeyboardState kbState)
        {
            var time = (float)gameTime.ElapsedGameTime.TotalSeconds;


            if (kbState.IsKeyDown(Keys.Left))
            {
                position.X -= speed;
             
            }
            if (kbState.IsKeyDown(Keys.Right))
            {
                position.X += speed;
            }
        }
    }
}
