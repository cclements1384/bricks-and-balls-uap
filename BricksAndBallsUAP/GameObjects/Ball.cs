﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace BricksAndBallsUAP.GameObjects
{
    public class Ball : GameObject
    {
        private Texture2D ball;
        public Vector2 position;
        public Vector2 direction;
       
        public float speed;
       
        public int Radius
        {
            get
            {
                /* Assumes ball texture is a square
                exactly the radius of the ball */
                return ball.Width /2;
            }
        }

        /// <summary>
        /// This represents the Y of the bottom
        /// of the texture in screen coordinates
        /// </summary>
        public int Bottom
        {
            get
            {
                return (int)position.Y + Radius;
            }
        }

        /// <summary>
        /// This represents the Y of the top
        /// of the texture in screen coordinates
        /// </summary>
        public int Top
        {
            get
            {
                return (int)position.Y;
            }
        }

        /// <summary>
        /// This represents the X of the left
        /// of the texture in screen coordinates
        /// </summary>
        public int Left
        {
            get
            {
                return (int)position.X;
            }
        }

        /// <summary>
        /// This represents the X of the right
        /// of the texture in screen coordinates
        /// </summary>
        public int Right
        {
            get
            {
                return (int)position.X + Radius;
            }
        }

        public Point center
        {
            get
            {
                var x = (int)(position.X + ball.Width / 2);
                var y = (int)(position.Y + ball.Height / 2);
                return new Point(x, y);
            }
        }

        public override void Initalize()
        {
            position = new Vector2(635.0f, 600.0f);
          
            /* Randomize the angle */
            //var gen = new System.Random((int)DateTime.Now.Ticks);
            //var x = gen.Next(-5, 5);
            direction = new Vector2(1.0f, -1.0f);


            speed = 250.0f;
        }

        public override void LoadContent(ContentManager content)
        {
            ball = content.Load<Texture2D>("graphics/ball");
        }

        public override void Update(GameTime gameTime)
        {
            position += direction * speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(ball, position, Color.Wheat);
        }

    }
}
