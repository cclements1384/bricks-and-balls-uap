﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace BricksAndBallsUAP.GameObjects.Screens.StartScreen
{
    public class StartButton : Button
    {
        public bool WasPressed(ref MouseState mouseState)
        {
            var mousePosition = new Point(mouseState.X,
                mouseState.Y);

            var leftButtonState = mouseState.LeftButton;

            if (leftButtonState == ButtonState.Pressed)
            {
                if (_location.Contains(mousePosition))
                {
                    return true;
                }
            }
            return false;
        }
    }
}
