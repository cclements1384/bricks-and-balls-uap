﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BricksAndBallsUAP.GameObjects.Screens.GameScreen
{
    public class GameScreen : GameObject
    {
        private Texture2D background;
        private Texture2D lifeBall;

        private Rectangle location;
        private Rectangle gamePort;

        private SpriteFont gameFont;

        private Vector2 position;
        private Vector2 scorePosition;
        private Vector2 lovePosition;

        private int livesRemaining;
        private int score;
        private const int MAX_LEVELS = 10;
        private int currentLevel;

        public Paddle paddle;
        public Ball ball;
        public Level level;

        private SoundEffect wallBounce;
        private SoundEffect dead;
        private SoundEffect paddleBounce;
        private SoundEffect brickBreak;
        
        public bool GameOver
        {
            get { return (livesRemaining < 0); }
        }

        public override void Initalize()
        {
            InitGamePort();
            scorePosition = new Vector2(500.0f, 25.0f);
            position = new Vector2(0.0f, 0.0f);
            paddle = new Paddle();
            paddle.Initalize();
            ball = new Ball();
            ball.Initalize();
            level = new Level();
            livesRemaining = 2;
            score = 0;
            currentLevel = 1;
        }

        public override void LoadContent(ContentManager content)
        {
            background = content.Load<Texture2D>("graphics/background1");
            location = new Rectangle(0, 
                0, 
                background.Width, 
                background.Height);

            paddle.LoadContent(content);
            ball.LoadContent(content);

            /* Load from XML */
            level.LoadLevel(currentLevel);
            /* Load textures */
            level.LoadContent(content);

            gameFont = content.Load<SpriteFont>("fonts/gamefont");
            lifeBall = content.Load<Texture2D>("graphics/ball");
            wallBounce = content.Load<SoundEffect>("sounds/fx236");
            dead = content.Load<SoundEffect>("sounds/fx238");
            paddleBounce = content.Load<SoundEffect>("sounds/fx225");
            brickBreak = content.Load<SoundEffect>("sounds/fx229");
        }

        public void Update(GameTime gameTime, KeyboardState kbState)
        {
            /* move the paddle */
            paddle.Update(gameTime, kbState);
            
            /* lock the paddle to the gamePort */
            paddle.position.X = MathHelper.Clamp(paddle.position.X, gamePort.Left, (gamePort.Right - paddle.Width));

            /* move the ball */
            ball.Update(gameTime);

            /* check if the ball hit the top */
            if (BallHitTop(ball))
            {
                wallBounce.Play();
                ball.direction.Y *= -1;
            }

            /* check if the ball hit the bottom */
            if (BallHitBottom(ball))
            {
                ball.direction.Y *= -1;
                LoseLife();
            }

            /* check if the ball hit the right side */
            if (BallHitRightSide(ball))
            {
                wallBounce.Play();
                ball.direction.X *= -1;
            }

            /* check if the ball hit the left side */
            if (BallHitLeftSide(ball))
            {
                wallBounce.Play();
                ball.direction.X *= -1;
            }

            /* check if the ball hit the paddle */
            if (BallHitPaddle(ball))
            {
                paddleBounce.Play();
                ball.direction = CalculateRebound(ball, paddle);
            }

            ///* check if the ball hit any bricks */
            Block blockToRemove = null;
            foreach(var brick in level.blocks)
            {
                if (BallHitBrickLeft(ball, brick))
                {
                    /* Deal damage to the brick */
                    brick.HitPoints--;
                    /* bounce off */
                    ball.direction.X *= -1;
                }
                else if (BallHitBrickRight(ball, brick))
                {
                    /* Deal damage to the brick */
                    brick.HitPoints--;
                    /* bounce off */
                    ball.direction.X *= -1;
                }
                else if (BallHitBrickTop(ball, brick))
                {
                    /* Deal damage to the brick */
                    brick.HitPoints--;
                    /* bounce off */
                    ball.direction.Y *= -1;
                }
                else if (BallHitBrickBottom(ball, brick))
                {
                    /* Deal damage to the brick */
                    brick.HitPoints--;
                    /* bounce off */
                    ball.direction.Y *= -1;
                }

                if(brick.HitPoints <= 0)
                {
                    brickBreak.Play();
                    blockToRemove = brick;
                    score += 100;
                    /* increase the speed with ever broken brick */
                    ball.speed += 5.0f;
                }
            }
            level.blocks.Remove(blockToRemove);

            /* Check to see if we cleared the level */
            if (level.IsCompleted)
                LoadNextLevel();

        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, location, Color.Wheat);
            DrawScore(spriteBatch);
            paddle.Draw(spriteBatch);
            ball.Draw(spriteBatch);
            level.Draw(spriteBatch);
        }


        #region Private Methods
        private void InitGamePort()
        {
            gamePort = new Rectangle(
             271,
             73,
             745,
             574);
        }

        private bool BallHitTop(Ball testBall)
        {
            if(testBall.Top <= gamePort.Top)
            {
                return true;
            }
            return false;
        }

        private bool BallHitBottom(Ball testBall)
        {
            if (testBall.Bottom >= gamePort.Bottom)
            {
                return true;
            }
            return false;
        }

        private bool BallHitLeftSide(Ball testBall)
        {
            if (testBall.Left <= gamePort.Left)
            { 
               return true;
            }
            return false;
        }

        private bool BallHitRightSide(Ball testBall)
        {
            if (testBall.Right >= gamePort.Right)
            {
                return true;
            }
            return false;
        }

        private bool BallHitPaddle(Ball testBall)
        {
            if((ball.center.X >= (paddle.Left - ball.Radius)) &&
                    (ball.center.X <= (paddle.Right + ball.Radius)) &&
                    (ball.Bottom >= (paddle.Top)))
            {
                var colliding = true;
                var msg = string.Format("Ball.Left:{0} Ball.Right:{1} Ball.Top:{2} Ball.Bottom:{3} -- Paddle.Left:{4} Paddle.Right:{5} Paddle.Top:{6} Paddle.Bottom:{7}",
                    ball.Left, ball.Right, ball.Top, ball.Bottom,
                    paddle.Left, paddle.Right, paddle.Top, paddle.Bottom);
                Debug.WriteLine(colliding, msg);
                return colliding;
            }
            return false;
        }


        private bool BallHitBrickRight(Ball ball, Block block)
        {
            /* Check if the ball hit the left side of the brick */
            if ((ball.center.X < block.Left) &&
                ((block.Left - ball.center.X) <= ball.Radius) &&
                (ball.center.Y <= block.Bottom) &&
                (ball.center.Y >= block.Top))
            {
                Debug.WriteLine("Left Collision");
                return true;
            }
            return false;
        }

        private bool BallHitBrickLeft(Ball ball, Block block)
        {
            if ((ball.center.X > block.Right) &&
                   ((ball.center.X - block.Right) <= ball.Radius) &&
                   (ball.center.Y <= block.Bottom) &&
                   (ball.center.Y >= block.Top))
            {
                Debug.WriteLine("Right Collision");
                return true;
            }
            return false;
        }

        private bool BallHitBrickTop(Ball ball, Block block)
        {
            if ((ball.center.X >= (block.Left)) &&
                   (ball.center.X <= (block.Right)) &&
                   (Math.Abs(block.Top - ball.center.Y) <= ball.Radius))
            {
                Debug.WriteLine("Top Collision");
                return true;
            }
            return false;
        }

        private bool BallHitBrickBottom(Ball ball, Block block)
        {
            if ((ball.center.X >= (block.Left)) &&
                (ball.center.X <= (block.Right)) &&
                (Math.Abs(ball.center.Y - block.Bottom) <= ball.Radius))
            {
                Debug.WriteLine("Bottom Collision");
                return true;
            }
            return false;
        }
       
        private Vector2 CalculateRebound(Ball testBall,Paddle testPaddle)
        {
            Vector2 reflectVector = Vector2.Zero;

            /* where did we hit the paddle
             * left third (-0.196f, -0.981f)
             * center third (0,-1)
             * right third (0.196f, 0.981f)
             */
            if ((ball.center.X >= paddle.Left) &&
                (ball.center.X <= (paddle.Left + (paddle.Width * .33))))
            {
                /* Left third of paddle */
                //reflectVector = new Vector2(-0.196f, -0.981f);
                reflectVector = new Vector2(-1.196f, -0.981f);
                Debug.WriteLine("Left Third");
            }
            else if ((ball.center.X >= (paddle.Left + (paddle.Width * .33))) &&
                (ball.center.X <= (paddle.Right - (paddle.Width * .33))))
            {
                /* Center third of paddle */
                reflectVector = new Vector2(0f, -1f);
                Debug.WriteLine("Center Third");
            }
            else if ((ball.center.X >= (paddle.Right - (paddle.Width * .33))) &&
                (ball.center.X <= paddle.Right))
            {
                /* Right third of paddle */
                //reflectVector = new Vector2(0.196f, -0.981f);
                reflectVector = new Vector2(1.196f, -0.981f);
                Debug.WriteLine("Right Third");
            }

            return reflectVector;
        }

        private void LoseLife()
        {
            dead.Play();
            livesRemaining--;
            ball.position = new Vector2(635.0f, 600.0f);
            ball.direction = new Vector2(1.0f, -1.0f);
        }

        private bool LevelCleared()
        {
            if (level.blocks.Count == 0)
                return true;
            return false;
        }

        private void DrawScore(SpriteBatch spriteBatch)
        {
            // build are score string
            var score = string.Concat("Score: ", this.score.ToString());
           

            // (NOT CURRENTLY USING THIS) find the center of the string
            Vector2 fontOrigin = gameFont.MeasureString(score) / 2;

            var dummyTexture = new Texture2D(spriteBatch.GraphicsDevice, 1, 1);
            dummyTexture.SetData(new Color[] { Color.White });

            var pos = new Vector2(400.0f, 10.0f);

            int bw = 2; // Border width
            spriteBatch.Draw(dummyTexture, new Rectangle(410, 10, 460,35), Color.Black);

            
            // draw the font.
            spriteBatch.DrawString(gameFont, score, scorePosition,
                Color.White, 0f, fontOrigin, 1.0f, SpriteEffects.None, 0.5f);

            var ballsPosition = new Vector2(700.0f, 25.0f);

            var balls = "Balls: ";
            Vector2 fontOrigin2 = gameFont.MeasureString(balls) / 2;
            spriteBatch.DrawString(gameFont, balls, ballsPosition,
              Color.White, 0f, fontOrigin2, 1.0f, SpriteEffects.None, 0.5f);

            var offset = 0;
            /* draw the balls for the lives */
            for (var i = 0; i < livesRemaining; i++)
            {
                spriteBatch.Draw(lifeBall, new Rectangle( 750 + offset , 20, 15, 15), Color.White);
                offset += 15;
            }

        }

        private void LoadNextLevel()
        {
            if (currentLevel < MAX_LEVELS)
            {
                var newlevel = currentLevel + 1;
                level.LoadLevel(newlevel);
                currentLevel = newlevel;
            }
        }


        #endregion
    }
}
