﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;

namespace BricksAndBallsUAP.GameObjects.Screens.StartScreen
{
    public class StartScreen : GameObject
    {
        private Texture2D texture;
        public StartButton startButton { get; set; }

        public override void Initalize()
        {
            startButton = new StartButton();
            startButton.Position = new Vector2(530, 450);
            base.Initalize();
        }

        public override void LoadContent(ContentManager content)
        {
            texture = content.Load<Texture2D>("graphics/menuscreen");
            startButton.Texture = content.Load<Texture2D>("graphics/startbutton");
            base.LoadContent(content);
        }

        public void Update(GameTime gameTime, ref MouseState mouseState)
        {
            if(startButton.WasPressed(ref mouseState))
            {
                var pressed = true;
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, new Microsoft.Xna.Framework.Vector2(0.0f, 0.0f), Microsoft.Xna.Framework.Color.Wheat);
            startButton.Draw(spriteBatch);
            base.Draw(spriteBatch);
        }
    }
}
